'use strict';
/**
 * Created by Juan on 21/11/2014.
 */
/*global io:false */

//socket factory that provides the socket service
angular.module('core').factory('Socket', ['socketFactory',
    function(socketFactory) {
        return socketFactory({
            prefix: '',
            ioSocket: io.connect('http://localhost:3000')
            //ioSocket: io.connect('http://schoolbus.herokuapp.com')
            //ioSocket: io.connect('http://powerful-caverns-1154.herokuapp.com')
        });
    }
]);
