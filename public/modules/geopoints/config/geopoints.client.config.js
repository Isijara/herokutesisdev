'use strict';

// Configuring the Articles module
angular.module('geopoints').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Localizar unidad', 'geopoints/track', '/geopoints/track');
		//Menus.addSubMenuItem('topbar', 'geopoints', 'List Geopoints', 'geopoints');
		//Menus.addSubMenuItem('topbar', 'geopoints', 'New Geopoint', 'geopoints/create');
	}
]);