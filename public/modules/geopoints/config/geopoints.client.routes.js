'use strict';

//Setting up route
angular.module('geopoints').config(['$stateProvider',
	function($stateProvider) {
		// Geopoints state routing
		$stateProvider.
		state('listGeopoints', {
			url: '/geopoints',
			templateUrl: 'modules/geopoints/views/list-geopoints.client.view.html'
		}).
		state('createGeopoint', {
			url: '/geopoints/track',
			templateUrl: 'modules/geopoints/views/create-geopoint.client.view.html'
		}).
		state('viewGeopoint', {
			url: '/geopoints/:geopointId',
			templateUrl: 'modules/geopoints/views/view-geopoint.client.view.html'
		}).
		state('editGeopoint', {
			url: '/geopoints/:geopointId/edit',
			templateUrl: 'modules/geopoints/views/edit-geopoint.client.view.html'
		});
	}
]);