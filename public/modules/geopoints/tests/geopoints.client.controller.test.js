'use strict';

(function() {
	// Geopoints Controller Spec
	describe('Geopoints Controller Tests', function() {
		// Initialize global variables
		var GeopointsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Geopoints controller.
			GeopointsController = $controller('GeopointsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Geopoint object fetched from XHR', inject(function(Geopoints) {
			// Create sample Geopoint using the Geopoints service
			var sampleGeopoint = new Geopoints({
				name: 'New Geopoint'
			});

			// Create a sample Geopoints array that includes the new Geopoint
			var sampleGeopoints = [sampleGeopoint];

			// Set GET response
			$httpBackend.expectGET('geopoints').respond(sampleGeopoints);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.geopoints).toEqualData(sampleGeopoints);
		}));

		it('$scope.findOne() should create an array with one Geopoint object fetched from XHR using a geopointId URL parameter', inject(function(Geopoints) {
			// Define a sample Geopoint object
			var sampleGeopoint = new Geopoints({
				name: 'New Geopoint'
			});

			// Set the URL parameter
			$stateParams.geopointId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/geopoints\/([0-9a-fA-F]{24})$/).respond(sampleGeopoint);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.geopoint).toEqualData(sampleGeopoint);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Geopoints) {
			// Create a sample Geopoint object
			var sampleGeopointPostData = new Geopoints({
				name: 'New Geopoint'
			});

			// Create a sample Geopoint response
			var sampleGeopointResponse = new Geopoints({
				_id: '525cf20451979dea2c000001',
				name: 'New Geopoint'
			});

			// Fixture mock form input values
			scope.name = 'New Geopoint';

			// Set POST response
			$httpBackend.expectPOST('geopoints', sampleGeopointPostData).respond(sampleGeopointResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Geopoint was created
			expect($location.path()).toBe('/geopoints/' + sampleGeopointResponse._id);
		}));

		it('$scope.update() should update a valid Geopoint', inject(function(Geopoints) {
			// Define a sample Geopoint put data
			var sampleGeopointPutData = new Geopoints({
				_id: '525cf20451979dea2c000001',
				name: 'New Geopoint'
			});

			// Mock Geopoint in scope
			scope.geopoint = sampleGeopointPutData;

			// Set PUT response
			$httpBackend.expectPUT(/geopoints\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/geopoints/' + sampleGeopointPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid geopointId and remove the Geopoint from the scope', inject(function(Geopoints) {
			// Create new Geopoint object
			var sampleGeopoint = new Geopoints({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Geopoints array and include the Geopoint
			scope.geopoints = [sampleGeopoint];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/geopoints\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleGeopoint);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.geopoints.length).toBe(0);
		}));
	});
}());