'use strict';

// Geopoints controller
angular.module('geopoints').controller('GeopointsController', ['$scope', '$stateParams', '$location', 'Socket', 'Authentication', 'Geopoints',
	function($scope, $stateParams, $location, Socket, Authentication, Geopoints) {
		window.$scope = $scope;

		$scope.trackingPath = [];
		$scope.map = { center: { latitude: 24.81456375, longitude: -107.41073161 }, zoom: 11 };
		$scope.marker = {
			id: 0,
			coords: {
				latitude: 24.81456375,
				longitude: -107.41073161
			},
			options: { draggable: false },
			events: {
				dragend: function (marker, eventName, args) {
					//$log.log('marker dragend');
					var lat = marker.getPosition().lat();
					var lon = marker.getPosition().lng();
					//$log.log(lat);
					//$log.log(lon);

					$scope.marker.options = {
						draggable: false,
						labelContent: 'Speed kph: 0.00 ',
						labelAnchor: '100 0',
						labelClass: 'marker-labels'
					};
				}
			}
		};
		$scope.megaline = [
            {
                id: 1,
                path: [{ latitude: 24.81456375, longitude: -107.41073161 }, { latitude: 24.81456375, longitude: -107.31073161 }],
				stroke: {
					color: '#6060FB',
					weight: 3
				},
				editable: false,
				draggable: false,
				geodesic: true,
				visible: $scope.showPolyline,
				icons: [{
					icon: {
						path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW
					},
					offset: '25px',
					repeat: '50px'
				}]
			}
		];

		$scope.mensajeArray = [];
		$scope.latitudeRec = '24.81456375';
		$scope.longitudeRec = '-107.41073161';
		$scope.velocidad = 'Speed Kph: 0.00';


		Socket.on('dato_recibido', function(msg){
			$scope.mensajeArray = msg.split(',');
			var lat = Number($scope.mensajeArray[3]);
			$scope.vendor = lat;
			var arreglo;
			lat /= 100;
			lat = lat.toFixed(6);
			arreglo = lat.toString().split('.');
			$scope.typeofthing = arreglo[0];
			$scope.classification = arreglo[1];

			var latdecimal = Number(arreglo[1]);
			latdecimal /= 1000000;
			$scope.latitude = latdecimal;

		});

		Socket.on('dato.recibido', function(msg){
			$scope.mensajeArray = msg.split(',');
			console.log(msg);

			//var latitudRecibida = parseFloat($scope.mensajeArray[3]);
			var latitudRecibida = Number($scope.mensajeArray[3]);
			var latitudRecibidaCadena = '';
			var latitudGrados = 0.0;
			var latitudMinutos = 0.0;
			var longitudRecibida = parseFloat($scope.mensajeArray[4]);
			var longitudRecibidaCadena = '';
			var longitudGrados = 0.0;
			var longitudMinutos = 0.0;
			var velocidadCadena = '';
			var arreglo = [];


			latitudRecibida /= 100;
			latitudRecibida = latitudRecibida.toFixed(8);
			arreglo = latitudRecibida.toString().split('.');
			latitudGrados = Number(arreglo[0]);
			latitudMinutos = Number(arreglo[1]);
			latitudMinutos /= 1000000;
			latitudMinutos /= 60;
			if(latitudGrados > 0){
				$scope.latitudeRec = (latitudGrados + latitudMinutos).toString();
			}else{
				$scope.latitudeRec = (latitudGrados - latitudMinutos).toString();
			}

			longitudRecibida /= 100;
			longitudRecibida = longitudRecibida.toFixed(8);
			longitudRecibidaCadena = longitudRecibida.toString();
			arreglo = longitudRecibidaCadena.split('.');
			longitudGrados = parseFloat(arreglo[0]);
			longitudMinutos = (parseFloat(arreglo[1]/1000000)/60);
			longitudMinutos = longitudMinutos.toFixed(8);
			if(longitudGrados > 0){
				$scope.longitudeRec = (longitudGrados + longitudMinutos).toString();
			}else{
				$scope.longitudeRec = (longitudGrados - longitudMinutos).toString();
			}

			$scope.marker.coords = {
				latitude: parseFloat($scope.latitudeRec),
				longitude: parseFloat($scope.longitudeRec)
			};
			$scope.map.center =  {
				latitude: parseFloat($scope.latitudeRec),
				longitude: parseFloat($scope.longitudeRec)
			};
			$scope.marker.options.labelContent = 'Speed Kph: ' + $scope.mensajeArray[7];



			//Agregando nuevo dato al trazado de la ruta

			$scope.trackingPath.push({
				latitude: $scope.latitudeRec,
				longitude: $scope.longitudeRec
			});

			if($scope.trackingPath.length > 1) $scope.megaline[0].path = $scope.trackingPath;



//{ latitude: 24.81456375, longitude: -107.41073161},
//{ latitude: 24.83456375, longitude: -107.41073161}

			//$scope.center = '[' + $scope.latitudeRec + ',' + $scope.longitudeRec + ']';
			velocidadCadena = 'Speed Kph: ' + $scope.mensajeArray[7];
			$scope.velocidad = velocidadCadena;

			var geopoint = new Geopoints ({
				//vendor: msg,
				vendor: $scope.mensajeArray[0],
				typeofthing: $scope.mensajeArray[1],
				classification: $scope.mensajeArray[2],
				//latitude: $scope.mensajeArray[3],
				latitude: $scope.latitudeRec,
				//longitude: $scope.mensajeArray[4],
				longitude: $scope.longitudeRec,
				timetoconnect: $scope.mensajeArray[5],
				numsatellites: $scope.mensajeArray[6],
				speedkph: $scope.mensajeArray[7]
			});

			if($scope.mensajeArray[7]>50) {
				$scope.error = 'Ha superado los límites de velocidad';
			}


			// Redirect after save
			geopoint.$save(function(response) {

				$scope.vendor = $scope.mensajeArray[0];
				$scope.typeofthing = $scope.mensajeArray[1];
				$scope.classification = $scope.mensajeArray[2];
				$scope.latitude = $scope.latitudeRec;
				//$scope.latitude = $scope.mensajeArray[3];
				$scope.longitude = $scope.longitudeRec;
				$scope.timetoconnect = $scope.mensajeArray[5];
				$scope.numsatellites = $scope.mensajeArray[6];
				$scope.speedkph = $scope.mensajeArray[7];

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		});

		$scope.close = function(){
			$location.path('/');
		};

		$scope.authentication = Authentication;

		// Create new Geopoint
		$scope.create = function() {
			// Create new Geopoint object
			var geopoint = new Geopoints ({
				vendor: this.vendor,
				typeofthing: this.typeofthing,
				classification: this.classification,
				latitude: this.latitude,
				longitude: this.longitude,
				timetoconnect: this.timetoconnect,
				numsatellites: this.numsatellites,
				speedkph: this.speedkph
			});

			// Redirect after save
			geopoint.$save(function(response) {
				$location.path('geopoints/' + response._id);

				// Clear form fields
				$scope.vendor = '';
				$scope.typeofthing = '';
				$scope.classification = '';
				$scope.latitude = '';
				$scope.longitude = '';
				$scope.timetoconnect = '';
				$scope.numsatellites = '';
				$scope.speedkph = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Geopoint
		$scope.remove = function(geopoint) {
			if ( geopoint ) {
				geopoint.$remove();

				for (var i in $scope.geopoints) {
					if ($scope.geopoints [i] === geopoint) {
						$scope.geopoints.splice(i, 1);
					}
				}
			} else {
				$scope.geopoint.$remove(function() {
					$location.path('geopoints');
				});
			}
		};

		// Update existing Geopoint
		$scope.update = function() {
			var geopoint = $scope.geopoint;

			geopoint.$update(function() {
				$location.path('geopoints/' + geopoint._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Geopoints
		$scope.find = function() {
			$scope.geopoints = Geopoints.query();
		};

		// Find existing Geopoint
		$scope.findOne = function() {
			$scope.geopoint = Geopoints.get({
				geopointId: $stateParams.geopointId
			});
		};
	}
]);
