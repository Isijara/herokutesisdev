'use strict';

//Geopoints service used to communicate Geopoints REST endpoints
angular.module('geopoints').factory('Geopoints', ['$resource',
	function($resource) {
		return $resource('geopoints/:geopointId', { geopointId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);