'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Geopoint = mongoose.model('Geopoint'),
	_ = require('lodash');

/**
 * Create a Geopoint
 */
exports.create = function(req, res) {
	var geopoint = new Geopoint(req.body);
	geopoint.user = req.user;

	geopoint.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(geopoint);
		}
	});
};

/**
 * Show the current Geopoint
 */
exports.read = function(req, res) {
	res.jsonp(req.geopoint);
};

/**
 * Update a Geopoint
 */
exports.update = function(req, res) {
	var geopoint = req.geopoint ;

	geopoint = _.extend(geopoint , req.body);

	geopoint.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(geopoint);
		}
	});
};

/**
 * Delete an Geopoint
 */
exports.delete = function(req, res) {
	var geopoint = req.geopoint ;

	geopoint.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(geopoint);
		}
	});
};

/**
 * List of Geopoints
 */
exports.list = function(req, res) { 
	Geopoint.find().sort('-created').populate('user', 'displayName').exec(function(err, geopoints) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(geopoints);
		}
	});
};

/**
 * Geopoint middleware
 */
exports.geopointByID = function(req, res, next, id) { 
	Geopoint.findById(id).populate('user', 'displayName').exec(function(err, geopoint) {
		if (err) return next(err);
		if (! geopoint) return next(new Error('Failed to load Geopoint ' + id));
		req.geopoint = geopoint ;
		next();
	});
};

/**
 * Geopoint authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.geopoint.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
