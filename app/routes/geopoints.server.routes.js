'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var geopoints = require('../../app/controllers/geopoints.server.controller');

	// Geopoints Routes
	app.route('/geopoints')
		.get(geopoints.list)
		.post(users.requiresLogin, geopoints.create);

	app.route('/geopoints/:geopointId')
		.get(geopoints.read)
		.put(users.requiresLogin, geopoints.hasAuthorization, geopoints.update)
		.delete(users.requiresLogin, geopoints.hasAuthorization, geopoints.delete);

	// Finish by binding the Geopoint middleware
	app.param('geopointId', geopoints.geopointByID);
};
