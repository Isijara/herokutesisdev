'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Geopoint Schema
 */
var GeopointSchema = new Schema({
	vendor: {
		type: String,
		default: '',
		required: 'Please fill vendor',
		trim: true
	},
	typeofthing: {
		type: String,
		default: '',
		required: 'Please fill type of thing',
		trim: true
	},
	classification: {
		type: String,
		default: '',
		required: 'Please fill classification of thing',
		trim: true
	},
	latitude: {
		type: String,
		default: '',
		required: 'Please fill latitude',
		trim: true
	},
	longitude: {
		type: String,
		default: '',
		required: 'Please fill longitude',
		trim: true
	},
	timetoconnect: {
		type: String,
		default: '',
		required: 'Please fill time to connect',
		trim: true
	},
	numsatellites: {
		type: String,
		default: '',
		required: 'Please fill number of satellites connected',
		trim: true
	},
	speedkph: {
		type: String,
		default: '',
		required: 'Please fill the speed in kph',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Geopoint', GeopointSchema);
