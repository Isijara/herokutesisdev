'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Geopoint = mongoose.model('Geopoint'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, geopoint;

/**
 * Geopoint routes tests
 */
describe('Geopoint CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Geopoint
		user.save(function() {
			geopoint = {
				name: 'Geopoint Name'
			};

			done();
		});
	});

	it('should be able to save Geopoint instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Geopoint
				agent.post('/geopoints')
					.send(geopoint)
					.expect(200)
					.end(function(geopointSaveErr, geopointSaveRes) {
						// Handle Geopoint save error
						if (geopointSaveErr) done(geopointSaveErr);

						// Get a list of Geopoints
						agent.get('/geopoints')
							.end(function(geopointsGetErr, geopointsGetRes) {
								// Handle Geopoint save error
								if (geopointsGetErr) done(geopointsGetErr);

								// Get Geopoints list
								var geopoints = geopointsGetRes.body;

								// Set assertions
								(geopoints[0].user._id).should.equal(userId);
								(geopoints[0].name).should.match('Geopoint Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Geopoint instance if not logged in', function(done) {
		agent.post('/geopoints')
			.send(geopoint)
			.expect(401)
			.end(function(geopointSaveErr, geopointSaveRes) {
				// Call the assertion callback
				done(geopointSaveErr);
			});
	});

	it('should not be able to save Geopoint instance if no name is provided', function(done) {
		// Invalidate name field
		geopoint.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Geopoint
				agent.post('/geopoints')
					.send(geopoint)
					.expect(400)
					.end(function(geopointSaveErr, geopointSaveRes) {
						// Set message assertion
						(geopointSaveRes.body.message).should.match('Please fill Geopoint name');
						
						// Handle Geopoint save error
						done(geopointSaveErr);
					});
			});
	});

	it('should be able to update Geopoint instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Geopoint
				agent.post('/geopoints')
					.send(geopoint)
					.expect(200)
					.end(function(geopointSaveErr, geopointSaveRes) {
						// Handle Geopoint save error
						if (geopointSaveErr) done(geopointSaveErr);

						// Update Geopoint name
						geopoint.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Geopoint
						agent.put('/geopoints/' + geopointSaveRes.body._id)
							.send(geopoint)
							.expect(200)
							.end(function(geopointUpdateErr, geopointUpdateRes) {
								// Handle Geopoint update error
								if (geopointUpdateErr) done(geopointUpdateErr);

								// Set assertions
								(geopointUpdateRes.body._id).should.equal(geopointSaveRes.body._id);
								(geopointUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Geopoints if not signed in', function(done) {
		// Create new Geopoint model instance
		var geopointObj = new Geopoint(geopoint);

		// Save the Geopoint
		geopointObj.save(function() {
			// Request Geopoints
			request(app).get('/geopoints')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Geopoint if not signed in', function(done) {
		// Create new Geopoint model instance
		var geopointObj = new Geopoint(geopoint);

		// Save the Geopoint
		geopointObj.save(function() {
			request(app).get('/geopoints/' + geopointObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', geopoint.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Geopoint instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Geopoint
				agent.post('/geopoints')
					.send(geopoint)
					.expect(200)
					.end(function(geopointSaveErr, geopointSaveRes) {
						// Handle Geopoint save error
						if (geopointSaveErr) done(geopointSaveErr);

						// Delete existing Geopoint
						agent.delete('/geopoints/' + geopointSaveRes.body._id)
							.send(geopoint)
							.expect(200)
							.end(function(geopointDeleteErr, geopointDeleteRes) {
								// Handle Geopoint error error
								if (geopointDeleteErr) done(geopointDeleteErr);

								// Set assertions
								(geopointDeleteRes.body._id).should.equal(geopointSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Geopoint instance if not signed in', function(done) {
		// Set Geopoint user 
		geopoint.user = user;

		// Create new Geopoint model instance
		var geopointObj = new Geopoint(geopoint);

		// Save the Geopoint
		geopointObj.save(function() {
			// Try deleting Geopoint
			request(app).delete('/geopoints/' + geopointObj._id)
			.expect(401)
			.end(function(geopointDeleteErr, geopointDeleteRes) {
				// Set message assertion
				(geopointDeleteRes.body.message).should.match('User is not logged in');

				// Handle Geopoint error error
				done(geopointDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Geopoint.remove().exec();
		done();
	});
});